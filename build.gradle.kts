plugins {
    kotlin("jvm") version "1.3.61"
    id("com.github.johnrengelman.shadow").version("5.2.0")
}

group = "dev.glycine"
version = "1.3.61"

repositories {
    jcenter()
    maven ("https://jitpack.io")
    maven ("https://repo.locationtech.org/content/groups/releases")
    maven("https://oss.sonatype.org/content/repositories/snapshots")
    maven("https://papermc.io/repo/repository/maven-public/")
}

dependencies {
    compileOnly("net.md-5","bungeecord-api","1.15-SNAPSHOT")
    compileOnly("com.destroystokyo.paper","paper-api","1.15.1-R0.1-SNAPSHOT")
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core","1.3.3")
    implementation("com.github.hazae41","mc-kutils","master-SNAPSHOT")
    implementation("org.apache.commons","commons-lang3","3.9")
    implementation("org.projectlombok","lombok","1.18.10")
}

tasks{
    processResources {
        with(copySpec {
            from("src/main/resources")
            filter<org.apache.tools.ant.filters.ReplaceTokens>("tokens" to mapOf("version" to version))
        })
    }

    compileKotlin {
        kotlinOptions.jvmTarget = "12"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "12"
    }
}
